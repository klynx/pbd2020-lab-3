package si.uni_lj.fri.pbd.lab3.fragmentapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;

public class TitlesFragment extends ListFragment {
    private boolean mDualPane;
    private int mCurCheckPosition = 0;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayAdapter<String> listItems = new ArrayAdapter<String>(getActivity(),
                R.layout.support_simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.titles));
        setListAdapter(listItems);

        getListView().setOnItemClickListener((arg1, arg2, index, arg3) -> showDetails(index));

        View detailsFrame = getActivity().findViewById(R.id.details);
        mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        if (mDualPane) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            showDetails(mCurCheckPosition);
        }
    }


    private void showDetails(int index) {

        if (mDualPane && index == mCurCheckPosition) {
            return;
        }

        mCurCheckPosition = index;
        if (mDualPane) {
           getListView().setItemChecked(index, true);
           DetailsFragment details = DetailsFragment.newInstance(index);

            FrameLayout dls = (FrameLayout) getActivity().findViewById(R.id.details);
            dls.removeAllViews();

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.details, details).commit();
        } else {
            Intent intent = new Intent();
            intent.setClass(getActivity(), DetailsActivity.class);
            intent.putExtra("index", index);
            startActivity(intent);
        }
    }
}
